import { defineUserConfig } from 'vuepress'
import type { DefaultThemeOptions } from 'vuepress'

export default defineUserConfig<DefaultThemeOptions>({
    title: 'GitLab ❤️ VuePress',
    description: 'Vue-powered static site generator running on GitLab Pages',
    base: '/vuepress/',
    dest: 'public',

    themeConfig: {
        sidebar: [
            '/README.md',
            '/guide/README.md',
            '/guide/nested.md',
        ],
    },
});
